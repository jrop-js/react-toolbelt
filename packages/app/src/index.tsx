import * as React from "react";
import { show } from "react-toolbelt";

show((resolve, reject) => (
  <div>
    This is a component being shown for a result!
    <br />
    <button onClick={() => reject("Cancel was clicked")}>Cancel</button>
    <button onClick={() => resolve("Okay was clicked")}>Okay</button>
  </div>
)).then(msg => alert(msg), e => alert(`Error: ${e}`));
