import * as React from "react";
import * as ReactDOM from "react-dom";

export type DeferredResolve<T> = (value?: T | PromiseLike<T>) => any;
export type DeferredReject = (reason?: any) => any;
export type Deferred<T> = {
  resolve: DeferredResolve<T>;
  reject: DeferredReject;
  promise: Promise<T>;
};
export function defer<T>() {
  const deferred: Deferred<T> = {
    resolve: null,
    reject: null,
    promise: null
  };
  deferred.promise = new Promise((resolve, reject) => {
    deferred.resolve = resolve;
    deferred.reject = reject;
  });
  return deferred;
}

export type ShowElementFactory<T> = (
  resolve: DeferredResolve<T>,
  reject: DeferredReject
) => React.ReactElement<any>;
export type IShowOptions = {
  container?: HTMLElement;
  unmountDelay?: number;
};
export async function show<T>(
  factory: ShowElementFactory<T>,
  options?: IShowOptions
) {
  options = options || {};
  const deferred = defer<T>();

  let container;
  let unmountContainer = true;
  if (options.container) {
    container = options.container;
    unmountContainer = false;
  } else {
    container = document.createElement("div");
    document.body.appendChild(container);
    unmountContainer = true;
  }

  const element = factory(deferred.resolve, deferred.reject);
  ReactDOM.render(element, container);
  await deferred.promise.catch(e => null);
  setTimeout(() => {
    ReactDOM.unmountComponentAtNode(container);
    if (unmountContainer) document.body.removeChild(container);
  }, typeof options.unmountDelay !== "undefined" ? options.unmountDelay : 0);

  return deferred.promise;
}
